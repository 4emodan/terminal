package ru.navis.terminal;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BluetoothSelectActivity extends AppCompatActivity {
    @BindView(R.id.bluetooth_list) ListView bluetoothList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_selector);
        ButterKnife.bind(this);

        refresh();
    }

    private ArrayList<BluetoothDevice> findBluetoothDevices() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (bluetoothAdapter == null || bluetoothAdapter.getAddress() == null) {
            Log.e("agro", "Bluetooth is not supported");
            Toast.makeText(this, getResources().getString(R.string.bluetooth_is_not_supported), Toast.LENGTH_SHORT).show();
            return new ArrayList<>();
        }
        if (!bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.enable();
        }

        return new ArrayList<>(bluetoothAdapter.getBondedDevices());
    }

    public void onBackPressed(View view) {
        finish();
    }

    public void onRefreshPressed(View view) {
        refresh();
    }

    private void refresh() {
        final ArrayAdapter macAdapter = new BluetoothArrayAdapter(
                this,
                R.layout.item_spinner,
                findBluetoothDevices());
        bluetoothList.setAdapter(macAdapter);

        bluetoothList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position,
                                    long id) {
                Intent intent = new Intent();
                intent.putExtra("mac", ((BluetoothDevice) macAdapter.getItem(position)).getAddress());
                intent.putExtra("name", ((BluetoothDevice) macAdapter.getItem(position)).getName());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}

class BluetoothArrayAdapter extends ArrayAdapter<BluetoothDevice> {
    public BluetoothArrayAdapter(Context context, int textViewResourceId, ArrayList<BluetoothDevice> items) {
        super(context, R.layout.item_devie_selector, R.id.label, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = super.getView(position, convertView, parent);

        TextView view = (TextView) v.findViewById(R.id.label);
        view.setText(itemName(position));

        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View v = super.getDropDownView(position, convertView, parent);

        TextView view = (TextView) v.findViewById(R.id.label);
        view.setText(itemName(position));

        return v;
    }

    private String itemName(int position) {
        BluetoothDevice item = getItem(position);
        if (item != null) {
            return String.format("%s (%s)", getAliasName(item), item.getAddress());
        } else {
            return "N/A";
        }
    }

    private String getAliasName(BluetoothDevice device) {
        String deviceAlias = device.getName();

        // Try to get alias name of bluetooth device.
        // That name can be changed by tablet user and become different from standard name.
        // Hack is dirty, see http://stackoverflow.com/questions/20658142/getting-the-renamed-name-of-an-android-bluetoothdevice
        try {
            Method method = device.getClass().getMethod("getAliasName");
            if(method != null) {
                deviceAlias = (String)method.invoke(device);
            }
        } catch (Exception e) {
            deviceAlias = device.getName();
        }

        return deviceAlias;
    }
}