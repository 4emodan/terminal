package ru.navis.terminal;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import ru.navis.terminal.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

public class BluetoothSource implements SPPService.Consumer {
    // Connection would be considered dead if no data arrives for that amount of time
    private static long IDLE_INTERVAL = 10000;
    // Interval between device search attempts
    private static long SEARCH_POLL_INTERVAL = 1000;
    // How long to keep bluetooth off when restarting it
    private static long RESTART_INTERVAL = 1000;

    // On some hardware combinations bluetooth connection may stall, especially when you
    // connect/disconnect rapidly. Switching bluetooth off and on helps.
    private static int RECONNECT_ATTEMPTS_THRESHOLD = 5;
    private static int MAX_BLUETOOTH_RESTART_ATTEMPTS = 2;

    private String mac;
    private String name;

    CharsetDecoder decoder;
    private FileOutputStream logFile;

    private BluetoothAdapter bluetoothAdapter;
    private SPPService bluetoothService;
    private Context context;
    private BluetoothThread workerThread;

    private Set<Consumer> consumers = new HashSet<>();
    private String messageTail = new String();

    interface Consumer {
        void consume(String message);
        void onStatusChanged(boolean connected);
    }

    public BluetoothSource(Context context, String mac, String name) {
        this.context = context;
        this.mac = mac;
        this.name = name;

        decoder = Charset.forName("UTF-8").newDecoder();
        decoder.onMalformedInput(CodingErrorAction.REPLACE);
        decoder.onUnmappableCharacter(CodingErrorAction.REPLACE);

        bluetoothService = new SPPService(this, IDLE_INTERVAL);
        workerThread = new BluetoothThread(mac);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (bluetoothAdapter == null || bluetoothAdapter.getAddress() == null) {
            Log.e("navis", "Bluetooth is not supported");
            Toast.makeText(context, context.getResources().getString(R.string.bluetooth_is_not_supported), Toast.LENGTH_SHORT).show();
            return;
        }

        workerThread.start();
    }

    public void destroy() {
        workerThread.cancel();
        bluetoothService.stop();
    }

    public void subscribe(Consumer consumer) {
        consumers.add(consumer);
    }

    public void unsubscribe(Consumer consumer) {
        consumers.remove(consumer);
    }

    public void write(String message) throws IOException {
        bluetoothService.write(message.getBytes("UTF-8"));
    }

    public String getMac() {
        return mac;
    }

    public String getName() {
        return name;
    }

    public void setLogFile(@Nullable String filename) {
        if (filename != null) {
            if (logFile != null)
                setLogFile(null);

            try {
                File logDirectory = new File(filename).getParentFile();
                logDirectory.mkdirs();
                logFile = new FileOutputStream(filename);
            } catch (FileNotFoundException e) {
                Log.e("navis", "Couldn't open log to file: " + e);
                Toast.makeText(context, context.getResources().getString(R.string.error_writing_file), Toast.LENGTH_SHORT).show();
                logFile = null;
            }
        } else {
            if (logFile != null)
                try {
                    logFile.close();
                } catch (IOException e) {
                    Log.e("navis", "Couldn't close log: " + e);
                }
            logFile = null;
        }
    }

    public boolean logEnabled() {
        return logFile != null;
    }

    @Override
    public void consume(byte[] data, int bytes) {
        if (logFile != null) {
            try {
                logFile.write(data, 0, bytes);
            } catch (IOException e) {
                Log.e("navis", "Couldn't write log to file: " + e);
                Toast.makeText(context, context.getResources().getString(R.string.error_writing_file), Toast.LENGTH_SHORT).show();
                setLogFile(null);
            }
        }

        try {
            CharBuffer parsed = decoder.decode(ByteBuffer.wrap(data, 0, bytes));
            messageTail += parsed.toString();
        } catch (CharacterCodingException e) {
            Log.e("navis", "Unsupported encoding: " + e);
            return;
        }
        for (;;) {
            int index = messageTail.indexOf('\n');
            if (index == -1)
                break;

            if (index != 0) {
                for (Consumer c : consumers) {
                    c.consume(messageTail.substring(0, index - 1));
                }
            }

            messageTail = messageTail.substring(index + 1);
        }
    }

    private void sendStatus(boolean connected) {
        for (Consumer c : consumers) {
            c.onStatusChanged(connected);
        }
    }

    private class BluetoothThread extends Thread {
        private boolean running = true;
        private String mac;
        private Handler uiHandler;

        public BluetoothThread(String mac) {
            uiHandler = new Handler(Looper.getMainLooper());
            this.mac = mac;
        }

        public void run() {
            Looper.prepare();
            int reconnectAttempts = 0;
            while(running) {
                boolean connecting = true;
                try {
                    connect();

                    synchronized (bluetoothService) {
                        while(bluetoothService.getState() != SPPService.STATE_DISCONNECTED) {
                            if (connecting && bluetoothService.getState() == SPPService.STATE_CONNECTED) {
                                sendStatus(true);
                                connecting = false;
                                reconnectAttempts = -1;
                            }
                            bluetoothService.wait();
                        }
                    }

                    reconnectAttempts += 1;
                    if (reconnectAttempts > RECONNECT_ATTEMPTS_THRESHOLD && reconnectAttempts <= RECONNECT_ATTEMPTS_THRESHOLD + MAX_BLUETOOTH_RESTART_ATTEMPTS) {
                        Log.e("navis", "Bluetooth connection seems to be stalled. Reinitializing bluetooth.");
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, context.getResources().getString(R.string.bluetooth_stalled), Toast.LENGTH_SHORT).show();
                            }
                        });
                        bluetoothAdapter.disable();
                        try {
                            Thread.sleep(RESTART_INTERVAL);
                        } catch (InterruptedException interrupt) {
                        }
                    }
                } catch (Exception e) {
                    Log.e("navis", "Got exception in bluetooth background thread: " + e.toString());
                }
                try {
                    Thread.sleep(SEARCH_POLL_INTERVAL);
                } catch (InterruptedException interrupt) {}

                if (!connecting) {
                    sendStatus(false);
                }
            }
        }

        private void connect()  throws IOException {
            Log.i("agro", "Connecting to bluetooth receiver " + mac);
            uiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(
                                context,
                                String.format(context.getResources().getString(R.string.connecting_to), mac),
                                Toast.LENGTH_SHORT).show();
                    }
                });

            if (!bluetoothAdapter.isEnabled()) {
                bluetoothAdapter.enable();
            }

            BluetoothDevice gpsDevice = null;
            Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
            for (BluetoothDevice device : pairedDevices) {
                if (device.getAddress().equals(mac)) {
                    gpsDevice = device;
                }
            }

            if (gpsDevice == null) {
                throw new IOException("Bluetooth device has not been found");
            }

            bluetoothService.connect(bluetoothAdapter, gpsDevice);
        }

        public void cancel() {
            running = false;
        }
    }
}
