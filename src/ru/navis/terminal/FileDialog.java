package ru.navis.terminal;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.navis.terminal.utils.Utils;

public class FileDialog extends DialogFragment {
    @BindView(R.id.filename) EditText filenameEdit;
    private LogListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (LogListener) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.dialog_file, null);

        ButterKnife.bind(this, layout);

        filenameEdit.setText(Environment.getExternalStoragePublicDirectory("nmea").getPath() + "/" + Utils.getCurrentDateString() + ".nme");

        builder.setView(layout);
        builder.setMessage(R.string.write_file)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        BluetoothSource source = TerminalApplication.get().getSource();
                        source.setLogFile(filenameEdit.getText().toString());
                        listener.logStarted();

                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Do nothing
                    }
                });
        return builder.create();
    }

    public interface LogListener {
        public void logStarted();
    }
}
