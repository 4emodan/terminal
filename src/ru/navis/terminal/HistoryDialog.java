package ru.navis.terminal;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import java.util.Vector;

public class HistoryDialog extends DialogFragment {
    public interface HistoryListener {
        public void onSelected(String command);
    }

    private HistoryListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (HistoryListener)activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final String[] history =  getArguments().getStringArray("history");
        builder.setItems(
                history,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onSelected(history[which]);
                    }
                });

        return builder.create();
    }
}
