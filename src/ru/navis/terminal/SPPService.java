package ru.navis.terminal;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class SPPService {

    public interface Consumer {
        void consume(byte[] data, int bytes);
    }

    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;

    private static final String TAG = "SPPService";
    private static final UUID UUID_SPP = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private ConnectThread mConnectThread;
    private int mState;

    private Consumer consumer;

    private long mIdleInterval;

    public SPPService(Consumer consumer, long idleInterval) {
        mState = STATE_DISCONNECTED;
        mIdleInterval = idleInterval;
        this.consumer = consumer;
    }

    private synchronized void setState(int state) {
        Log.d(TAG, "setState() " + mState + " -> " + state);

        synchronized (this) {
            mState = state;
            notifyAll();
        }
    }

    public synchronized int getState() {
        return mState;
    }

    public synchronized void connect(BluetoothAdapter adapter, BluetoothDevice device) throws IOException {
        Log.d(TAG, "connect(" + device + ")");

        if (mState == STATE_CONNECTING || mState == STATE_CONNECTED) {
            resetConnectThread();
        }

        mConnectThread = new ConnectThread(adapter, device);
        mConnectThread.start();
        setState(STATE_CONNECTING);
    }

    public synchronized void stop() {
        Log.d(TAG, "stop()");

        resetConnectThread();
        setState(STATE_DISCONNECTED);
    }

    public void write(byte[] data) throws IOException {
        ConnectThread t;
        synchronized (this) {
            if (mState == STATE_CONNECTED)
                t = mConnectThread;
            else
                return;
        }
        t.write(data);
    }

    private synchronized void resetConnectThread() {
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }
    }

    private class ConnectThread extends Thread {

        private final BluetoothSocket mSocket;

        private InputStream mInputStream;
        private OutputStream mOutputStream;

        Watchdog watchdog = new Watchdog(mIdleInterval, new Runnable() {
            @Override
            public void run() {
                cancel();
                SPPService.this.stop();
            }
        });

        public ConnectThread(BluetoothAdapter adapter, BluetoothDevice device) throws IOException {
            Log.d(TAG, "ConnectThread(" + device + ")");
            mSocket = device.createRfcommSocketToServiceRecord(UUID_SPP);

            watchdog.start();

            adapter.cancelDiscovery();
        }

        public void run() {
            try {
                mSocket.connect();
            } catch (IOException e) {
                cancel("Failed to connect to the socket!", e);
                return;
            }

            setState(STATE_CONNECTED);

            try {
                mInputStream = mSocket.getInputStream();
                mOutputStream = mSocket.getOutputStream();
            } catch (IOException e) {
                cancel("I/O streams cannot be created from the socket!", e);
                return;
            }

            readData();
        }

        private void readData() {
            byte[] data = new byte[1024];
            int bytes;

            while (true) {
                try {
                    bytes = mInputStream.read(data);
                    consumer.consume(data, bytes);

                    watchdog.clear();
                } catch (IOException e) {
                    cancel("Failed to read data from socket.", e);
                    break;
                }
            }
        }

        public void write(byte[] data) throws IOException {
            mOutputStream.write(data);
        }

        public void cancel() {
            watchdog.cancel();
            try {
                mSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Unable to close the socket!");
            }
        }

        private void cancel(String message, Exception err) {
            Log.e(TAG, message + " " + err.toString());
            cancel();
            SPPService.this.stop();
        }
    }
}
