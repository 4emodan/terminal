package ru.navis.terminal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.SpanWatcher;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.LinkedHashSet;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TerminalActivity
        extends AppCompatActivity
        implements
        BluetoothSource.Consumer,
        HistoryDialog.HistoryListener,
        FileDialog.LogListener {
    private static final int REQUEST_BLUETOOTH_MAC = 1;
    private static final int MAX_LOG_LINES = 100;
    private static final int HISTORY_SIZE = 10;
    @BindView(R.id.log_scroll) ScrollView logScroll;
    @BindView(R.id.log_view) TextView logView;
    @BindView(R.id.command_edit) EditText commandEdit;
    @BindView(R.id.connect_button) Button connectButton;
    @BindView(R.id.device_name) TextView deviceNameView;
    @BindView(R.id.device_address) TextView deviceAddressView;
    @BindView(R.id.log_button) TextView logButton;
    @BindView(R.id.pause_button) TextView pauseButton;
    @BindView(R.id.device_info) View deviceInfoView;
    @BindView(R.id.not_connected) View notConnectedLabel;
    @BindView(R.id.send_button) ImageButton sendButton;
    private int logLineCount = 0;
    private boolean isPaused = false;
    private LinkedHashSet<String> history = new LinkedHashSet<>();

    @Override
    public void consume(final String message) {
        runOnUiThread(new Runnable() {
            public void run() {
                if (!isPaused) {
                    append(message, "");
                    scrollToBottom();
                }
            }
        });
    }

    @Override
    public void onStatusChanged(final boolean connected) {
        runOnUiThread(new Runnable() {
            public void run() {
                commandEdit.setEnabled(connected);
                if (connected) {
                    deviceNameView.setText(getSource().getName());
                    sendButton.setImageResource(R.drawable.ic_send_active_24_px);
                } else {
                    deviceNameView.setText(String.format(getResources().getString(R.string.connecting_to), getSource().getName()));
                    sendButton.setImageResource(R.drawable.ic_send_inactive_24_px);
                }
            }
        });
    }

    @Override
    public void onSelected(String command) {
        commandEdit.setText(command);
        commandEdit.setSelection(command.length());
    }

    @Override
    public void logStarted() {
        adjustLogButtonText();
    }

    @Override
    protected void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_terminal);
        ButterKnife.bind(this);

        logView.setGravity(Gravity.BOTTOM);

        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonArray array = parser.parse(getPreferences(MODE_PRIVATE).getString("history", "[]")).getAsJsonArray();
        for (int i = 0; i < array.size(); ++i) {
            history.add(gson.fromJson(array.get(i), String.class));
        }

        commandEdit.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        commandEdit.setOnEditorActionListener(
                new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            sendMessage();
                            return true;
                        }
                        return false;
                    }
                });
        commandEdit.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        for (int i = 0; i < editable.length(); ++i) {
                            if (Character.isLowerCase(editable.charAt(i))) {
                                editable.replace(i, i + 1, Character.toString(Character.toUpperCase(editable.charAt(i))));
                            }
                        }

                        if (editable.length() < 1 || editable.charAt(0) != '$')
                            editable.insert(0, "$");
                    }
                });
        final SpanWatcher watcher = new SpanWatcher() {
            @Override
            public void onSpanAdded(final Spannable text, final Object what, final int start, final int end) {
            }

            @Override
            public void onSpanRemoved(final Spannable text, final Object what, final int start, final int end) {
            }

            @Override
            public void onSpanChanged(final Spannable text, final Object what, final int ostart, final int oend, final int nstart, final int nend) {
                if (commandEdit.getSelectionEnd() < 1) {
                    commandEdit.setSelection(1, 1);
                }
                if (commandEdit.getSelectionStart() < 1) {
                    commandEdit.setSelection(1, commandEdit.getSelectionEnd());
                }
            }
        };
        commandEdit.getText().setSpan(watcher, 0, 0, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        commandEdit.setSelection(1);

        scrollToBottom();

        if (getSource() != null) {
            onConnected();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_BLUETOOTH_MAC:
                if (resultCode == RESULT_OK) {
                    connect(data.getStringExtra("mac"), data.getStringExtra("name"));
                }
                break;
        }
    }

    @Override
    protected void onPause() {
        if (getSource() != null) {
            getSource().unsubscribe(this);
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getSource() != null) {
            onConnected();
        }
    }

    public void onConnectPressed(View view) {
        if (getSource() == null) {
            Intent intent = new Intent(this, BluetoothSelectActivity.class);
            startActivityForResult(intent, REQUEST_BLUETOOTH_MAC);
        } else {
            disconnect();
        }
    }

    public void onFilePressed(View view) {
        BluetoothSource source = getSource();
        if (source != null && source.logEnabled()) {
            source.setLogFile(null);
        } else {
            DialogFragment dialog = new FileDialog();
            dialog.show(getSupportFragmentManager(), "FileDialog");
            logButton.setText(R.string.write_file);
        }
        adjustLogButtonText();
    }

    public void onPausePressed(View view) {
        if (getSource() != null) {
            pauseButton.setVisibility(View.VISIBLE);
            isPaused = true;
        }
    }

    public void onResumePressed(View view) {
        resetLogView();
    }

    public void onSendPressed(View view) {
        sendMessage();
    }

    private void connect(String mac, String name) {
        ((TerminalApplication) getApplication()).makeSource(mac, name);
        onConnected();
    }

    private void onConnected() {
        getSource().subscribe(this);

        resetLogView();

        notConnectedLabel.setVisibility(View.INVISIBLE);
        deviceInfoView.setVisibility(View.VISIBLE);
        deviceNameView.setText(String.format(getResources().getString(R.string.connecting_to), getSource().getName()));
        deviceAddressView.setText(getSource().getMac());

        connectButton.setText(R.string.disconnect);
        connectButton.setBackgroundColor(ContextCompat.getColor(this, R.color.disconnect_button));
        logButton.setEnabled(true);
    }

    private void disconnect() {
        if (getSource() != null) {
            getSource().unsubscribe(this);
            ((TerminalApplication) getApplication()).destroySource();
        }

        connectButton.setText(R.string.connect);
        connectButton.setBackgroundColor(ContextCompat.getColor(this, R.color.connect_button));
        logButton.setEnabled(false);

        notConnectedLabel.setVisibility(View.VISIBLE);
        deviceInfoView.setVisibility(View.INVISIBLE);
        deviceAddressView.setText("");

        commandEdit.setEnabled(false);
        sendButton.setImageResource(R.drawable.ic_send_inactive_24_px);
    }

    private void append(String message, String color) {
        logView.measure(0, 0);

        if (logLineCount != 0)
            logView.append("\n");
        logView.append(Html.fromHtml("<font color=\"" + color + "\">" + message + "</font>"));

        ++logLineCount;

        if (logLineCount > MAX_LOG_LINES) {
            --logLineCount;

            int lineEnd = logView.getEditableText().toString().indexOf('\n') + 1;
            logView.getEditableText().delete(0, lineEnd);
        }

        scrollToBottom();
    }

    private void scrollToBottom() {
        logView.post(new Runnable() {
            public void run() {
                logView.measure(0, 0);
                logScroll.scrollTo(0, logView.getHeight() - logScroll.getHeight());
            }
        });
    }

    private BluetoothSource getSource() {
        return ((TerminalApplication) getApplication()).getSource();
    }

    private void adjustLogButtonText() {
        BluetoothSource source = getSource();
        logButton.setText(source != null && source.logEnabled() ? R.string.stop_writing_file : R.string.write_file);
    }

    private void resetLogView() {
        logView.setMovementMethod(null);
        logView.setText("");
        logLineCount = 0;

        pauseButton.setVisibility(View.GONE);
        isPaused = false;
    }

    private void sendMessage() {
        if (getSource() == null)
            return;

        String message = commandEdit.getText().toString();
        if (message.equals("$")) {
            if (history.isEmpty())
                return;

            Bundle args = new Bundle();
            args.putStringArray("history", history.toArray(new String[history.size()]));
            DialogFragment dialog = new HistoryDialog();
            dialog.setArguments(args);
            dialog.show(getSupportFragmentManager(), "HistoryDialog");
            return;
        }

        if (isPaused)
            resetLogView();

        addToHistory(message);

        int crc = 0;
        for (int i = 1; i < message.length(); i++) {
            crc ^= message.charAt(i);
        }
        message = String.format("%s*%02X", message, crc);

        try {
            getSource().write(message + "\r\n");

            append(message, "#FF5555");
            scrollToBottom();
        } catch (IOException e) {
            Log.e("navis", "Couldn't send data to device: " + e.toString());
            Toast.makeText(TerminalActivity.this, getResources().getString(R.string.write_error), Toast.LENGTH_SHORT).show();
        }
    }

    private void addToHistory(String message) {
        history.remove(message);
        history.add(message);

        if (history.size() > HISTORY_SIZE)
            history.remove(history.iterator().next());

        getPreferences(MODE_PRIVATE).edit().putString("history", new Gson().toJson(history)).apply();
    }
}
