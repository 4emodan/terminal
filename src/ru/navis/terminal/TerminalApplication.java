package ru.navis.terminal;

import android.app.Application;
import android.os.Handler;
import android.os.HandlerThread;

import java.util.Timer;
import java.util.TimerTask;


public class TerminalApplication extends Application {

    private static TerminalApplication instance;

    private Handler backgroundHandler;
    private BluetoothSource source = null;

    private Timer mActivityTransitionTimer;
    private TimerTask mActivityTransitionTimerTask;
    public boolean wasInBackground = true;
    private final long MAX_ACTIVITY_TRANSITION_TIME_MS = 2000;

    public static TerminalApplication get() {
        return instance;
    }

    public Handler getBackgroundHandler() {
        return backgroundHandler;
    }

    public BluetoothSource getSource() {
        return source;
    }

    public void makeSource(String mac, String name) {
        destroySource();
        source = new BluetoothSource(this, mac, name);
    }

    public void destroySource() {
        if (source != null) {
            source.destroy();
            source = null;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        HandlerThread backgroundThread = new HandlerThread("Agro background thread");
        backgroundThread.start();
        backgroundHandler = new Handler(backgroundThread.getLooper());
	}
}
