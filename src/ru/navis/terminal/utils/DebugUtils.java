package ru.navis.terminal.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * @author Artyom Lonsky (artyom.lonsky@gmail.com)
 * @since 12.03.2016
 */
public class DebugUtils {

    private static final String TAG = DebugUtils.class.getName();
/*
    public static boolean copyDb(Context context) {
        // Don't copy if it's already in cache dir
        if (BuildConfig.DEBUG) {
            return true;
        }
        String from = TrackDatabaseHelper.getDatabasePath(context).getPath();
        return copyFile(from, context.getExternalCacheDir() + "/" + TrackDatabaseHelper.DATABASE_NAME);
    }*/

    @SuppressLint("DefaultLocale")
    public static boolean writeLog(Context context) {
        boolean result = false;

        String filePath = null;
        File cacheDir = context.getExternalCacheDir();
        if (cacheDir != null) {
            filePath = String.format("%s/%s_%s.txt",
                    cacheDir.getPath(),
                    "log",
                    Utils.getCurrentDateString());

            BufferedReader reader = null;
            OutputStreamWriter writer = null;
            try {
                Process process = Runtime.getRuntime().exec("logcat -d");
                reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

                writer = new FileWriter(filePath);

                String line;
                while ((line = reader.readLine()) != null) {
                    line += "\n";
                    writer.write(line);
                }
                result = true;
            } catch (IOException e) {
                Log.e(TAG, "Unable to write log to a file", e);
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException ignored) {
                    }
                }
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        } else {
            Log.e(TAG, "Cache directory unavailable!");
        }

        return result;
    }

    public static boolean copyFile(String fromPath, String toPath) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = new FileInputStream(fromPath);
            out = new FileOutputStream(toPath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            out.close();

            return true;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return false;
        }
    }

    private static byte[] readFileToByteArray(File file) throws IOException {
        int DEFAULT_BUFFER_SIZE = 128 * 1024;
        InputStream in = null;
        try {
            in = new FileInputStream(file);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int n = 0;
            while (-1 != (n = in.read(buffer))) {
                output.write(buffer, 0, n);
            }
            return output.toByteArray();
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

}
