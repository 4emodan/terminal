package com.agroexp.trac.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.DrawableRes;

/**
 * @author Artyom Lonsky (artyom.lonsky@gmail.com)
 * @since 03.02.2016
 */
public class ImageUtils {

    /**
     * @param fitHeight          desired height of the output image. Width would be adjusted keeping the aspect ratio.
     * @param mirrorHorizontally to mirror the tile horizontally
     */
    public static BitmapDrawable getTiledDrawable(Context context, @DrawableRes int resId,
                                                  int fitHeight, boolean mirrorHorizontally,
                                                  Shader.TileMode horizontalTileMode) {
        Resources res = context.getResources();

        BitmapFactory.Options options = getProcessingOptions(res, resId, fitHeight);
        Bitmap original = BitmapFactory.decodeResource(res, resId, options);
        original.setDensity(Bitmap.DENSITY_NONE);

        float ratio = (float) fitHeight / original.getHeight();
        int scaledWidth = (int) (original.getWidth() * ratio);

        Bitmap scaled = null;
        if (mirrorHorizontally) {
            Matrix matrix = new Matrix();
            matrix.preScale(-ratio, ratio);
            scaled = Bitmap.createBitmap(original, 0, 0, original.getWidth(), original.getHeight(), matrix, false);
        } else {
            scaled = Bitmap.createScaledBitmap(original, scaledWidth, fitHeight, true);
        }

        if (original != scaled) {
            original.recycle();
        }

        BitmapDrawable drawable = new BitmapDrawable(res, scaled);
        drawable.setTileModeX(horizontalTileMode);

        return drawable;
    }

    private static BitmapFactory.Options getProcessingOptions(Resources resources, int resourceId, int height) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = true;

        // Find out image bounds
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resources, resourceId, options);
        options.inJustDecodeBounds = false;

        int bmpHeight = options.outHeight;

        float ratio = (float) bmpHeight / height;

        if (ratio > 1f) {
            options.inSampleSize = (int) ratio;
        }

        return options;
    }

}
