package ru.navis.terminal.utils;

import android.content.Context;
import android.content.res.Resources;
import android.os.Vibrator;
import android.util.TypedValue;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
    public static long BUTTON_VIBRATE_TIME = 300;

    public static float getDimenValue(Resources resources, int id) {
        TypedValue outValue = new TypedValue();
        resources.getValue(id, outValue, true);
        return outValue.getFloat();
    }

    public static void vibrate(Context context, long time) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(time);
    }

    public static void vibrate(Context context) {
        vibrate(context, BUTTON_VIBRATE_TIME);
    }

    public static String getCurrentDateString() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        return sdf.format(new Date());
    }
}
